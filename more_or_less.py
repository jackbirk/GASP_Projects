from random import randint

num1 = randint(50, 100)
num2 = randint(50, 100)

question = "What is " + str(num1) + " plus " + str(num2) + "? "
user_answer = int(input(question))
correct_answer = num1 + num2

if user_answer == correct_answer:
    print("Good job, that's correct!")
else:
    print("I'm sorry, that's incorrect. The correct answer is " + str(correct_answer) + ".")


